﻿using System;
using System.IO;
using System.Linq;
using System.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using SignThis.CryptUI;
namespace SignThis
{
    class Program
    {
        static void Main(string[] args)
        {
            var filePath = args.Length > 0 ? new FileInfo(args[0]) : null;
            if (filePath != null && filePath.Exists)
            {
                try
                {
                    Helper.SignFileFromDisk(filePath);
                }

                catch (SecurityException)
                {
                    Console.WriteLine("No codesigning certificates found");
                }
                
            }
            else
            {
                Console.WriteLine("Please provide a valid filename on the command line");
            }
        }
    }
}
