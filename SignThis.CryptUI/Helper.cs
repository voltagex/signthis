﻿using System.Security;
using ClrPlus.Windows.Api;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace SignThis.CryptUI
{
    public class Helper
    {
        public static void SignFileFromDisk(FileInfo filePath)
        {
            X509Certificate2 signingCertificate = GetCodeSigningCertificate();

            if (signingCertificate == null)
            {
                throw new SecurityException("No signing certificate found");
            }

            const DigitalSignFlags flags = DigitalSignFlags.NoUI;
            DigitalSignInfo dsi = new DigitalSignInfo();
            IntPtr certificateHandle = signingCertificate.Handle;

            try
            {
                dsi.pwszFileName = filePath.FullName;
                dsi.dwSigningCertChoice = DigitalSigningCertificateChoice.Certificate;
                dsi.dwAdditionalCertChoice = DigitalSignAdditionalCertificateChoice.AddChainNoRoot;
                dsi.dwSubjectChoice = DigitalSignSubjectChoice.File;
                dsi.pwszTimestampURL = null;
                dsi.pSignExtInfo = IntPtr.Zero;
                dsi.pSigningCertContext = certificateHandle;
                dsi.dwSize = Marshal.SizeOf(dsi);
                bool result = CryptUi.CryptUIWizDigitalSign(flags, IntPtr.Zero, "", ref dsi, ref dsi.pSigningCertContext);

                if (!result)
                {
                    throw new Win32Exception(Marshal.GetLastWin32Error());
                }
            }

            finally
            {
                //CryptUi.CryptUIWizFreeDigitalSignContext(dsi.pSigningCertContext); 
                //- currently throws, from my reading of http://msdn.microsoft.com/en-us/library/windows/desktop/aa380292%28v=vs.85%29.aspx
                //I'm not doing any damage by not freeing it.
            }
        }

        //public static void SignFileFromBytes(byte[] bytes)
        //{
        //    X509Certificate2 signingCertificate = GetCodeSigningCertificate();

        //    if (signingCertificate == null)
        //    {
        //          throw new SecurityException("No signing certificate found");
        //    }
              


        //    const DigitalSignFlags flags = DigitalSignFlags.NoUI;
        //    DigitalSignInfo dsi = new DigitalSignInfo();
        //    IntPtr certificateHandle = signingCertificate.Handle;
        //    IntPtr unmanagedPointerToBlob = IntPtr.Zero;
        //    try
        //    {
                
        //        dsi.dwSigningCertChoice = DigitalSigningCertificateChoice.Certificate;
        //        dsi.dwAdditionalCertChoice = DigitalSignAdditionalCertificateChoice.AddChainNoRoot;
        //        dsi.dwSubjectChoice = DigitalSignSubjectChoice.Blob;

        //        unmanagedPointerToBlob = Marshal.AllocHGlobal(bytes.Length);
        //        Marshal.Copy(bytes, 0, unmanagedPointerToBlob, bytes.Length);

        //        var blob = new DigitalSignBlobInfo();
        //        blob.pGuidSubject = IntPtr.Zero;
        //        blob.pbBlob = unmanagedPointerToBlob;
        //        blob.dwSize = Marshal.SizeOf(bytes);
        //        blob.pwszDisplayName = "test.blob";

        //        dsi.pSignBlobInfo = blob;
        //        dsi.pwszTimestampURL = null;
        //        dsi.pSignExtInfo = IntPtr.Zero;
        //        dsi.pSigningCertContext = certificateHandle;
        //        dsi.dwSize = Marshal.SizeOf(dsi);
        //        bool result = CryptUi.CryptUIWizDigitalSign(flags, IntPtr.Zero, "", ref dsi, ref dsi.pSigningCertContext);

        //        if (!result)
        //        {
        //            throw new Win32Exception(Marshal.GetLastWin32Error());
        //        }
        //    }

        //    finally
        //    {
        //        Marshal.FreeHGlobal(unmanagedPointerToBlob);
        //    }
        //}





        private static X509Certificate2 GetCodeSigningCertificate()
        {
            X509Store store = new X509Store("My");

            store.Open(OpenFlags.ReadOnly);

            var certs = store.Certificates.Cast<X509Certificate2>().Where(x => x.HasCodeSigningOID()).ToList();

            return certs[0];
        }

    }
}
