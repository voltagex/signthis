﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace SignThis.CryptUI
{
    public static class CertificateExtensions
    {
        public static bool HasCodeSigningOID(this X509Certificate2 cert)
        {
            var xyz = cert.Extensions.OfType<X509Extension>();
            var zzz = cert as X509Certificate;

            var enhancedExtensions =
            cert.Extensions.OfType<X509EnhancedKeyUsageExtension>().Select(x => x.EnhancedKeyUsages).FirstOrDefault();

            if (enhancedExtensions != null)
            {
                var oids = enhancedExtensions.OfType<Oid>();
                return oids.Where(x => IsCodeSigningOID(x)).ToList().Count() > 0;
            }

            return false;
        }

        private static bool IsCodeSigningOID(Oid oid)
        {
            return oid.FriendlyName == "Code Signing";
        }
    }
}
